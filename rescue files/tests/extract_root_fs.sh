#!/bin/sh
tmp_dir=root_fs_dir
inputfile=rescue.root.emmc.cpio.gz_pad.img
mkdir ${tmp_dir}    
gunzip < ${inputfile} | cpio -idmv -D ${tmp_dir}
