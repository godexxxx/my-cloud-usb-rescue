#!/bin/sh
roofs_dir=root_fs_dir
needed_file_size=4194304
outputFile=rescue.root.emmc.cpio.gz_pad.img
#put in archive root fs dir
echo "creating rootfs"
(cd "${roofs_dir}"; find . | cpio -o -H newc | gzip) > ${outputFile}

#add null date to the end to get needed size (hack)
outputFile_size=$(wc -c < ${outputFile})
need_add_to_the_end=$((${needed_file_size} - ${outputFile_size}))
dd if=/dev/zero of=${outputFile} count=${need_add_to_the_end} seek=${outputFile_size} bs=1
mkdir /media/bohdan_pavlenko/usb/bin
cp ${outputFile} /media/bohdan_pavlenko/usb
rm /media/bohdan_pavlenko/usb/bin/*
cp ./bin/* /media/bohdan_pavlenko/usb/bin
cp ./{rescue.emmc.dtb,bluecore.audio,emmc.uImage} /media/bohdan_pavlenko/usb