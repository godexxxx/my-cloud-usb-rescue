#!/bin/bash
source config.sh

echo "# /etc/fstab: static file system information.
#
# Use 'vol_id --uuid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>      <options>       <dump>  <pass>
proc            /proc           proc        defaults        0       0 " > ${TARGET}/etc/fstab

echo "deb http://http.debian.net/debian $DISTRO main contrib non-free
# deb-src http://http.debian.net/debian $DISTRO main
 
deb http://security.debian.org $DISTRO/updates main contrib non-free
# deb-src http://security.debian.org $DISTRO/updates main
 
#deb http://http.debian.net/debian-backports $DISTRO-backports main non-free
# deb-src http://http.debian.net/debian-backports $DISTRO-backports main
 
#deb http://http.debian.net/debian $DISTRO-proposed-updates main contrib non-free
# deb-src http://http.debian.net/debian $DISTRO-proposed-updates main" > ${TARGET}/etc/apt/sources.list

cp /usr/bin/qemu-aarch64-static ${TARGET}/usr/bin # add qemu
cp postinst_script.sh ${TARGET}/
#cp udhcpc.script $TARGET/etc/udhcpc.script

## строчки ниже трогать не нужно, они монтируют системные директории в новый /

mount -o bind /dev ${TARGET}/dev
mount -o bind /sys ${TARGET}/sys

sudo env LANG=C env HOME=/root chroot ${TARGET}/ qemu-aarch64-static /bin/bash /postinst_script.sh

umount -R ${TARGET}/dev
umount -R ${TARGET}/sys
umount -R ${TARGET}/proc
mount
