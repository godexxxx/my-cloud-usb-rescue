#!/bin/bash

## update repo
apt-get update

## Config locale
locale-gen --purge en_US.UTF-8
LANG=en_US.UTF-8
apt-get install -y locales && \
    sed -i -e "s/# $LANG.*/$LANG.UTF-8 UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=$LANG


## install some deps
echo "install debs"
apt-get -y -V \
        install libstat-lsmode-perl \
        mdadm net-tools udhcpc telnetd ipsvd bash-completion \
        ca-certificates salt-common \
        gnupg lvm2  \
        openssh-server

## Config time
echo "Europe/London" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

## mount fs
mount -t proc /proc /proc
mount -a

# bash aoto complite
echo "
if [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
" > /etc/bash.bashrc

## set hostname
HOST='mycloudduoomv'

echo "$HOST" > /etc/hostname
echo -e "\n127.0.0.1 localhost $HOST" >> /etc/hosts

## add user and root access to this user
USER='omv'
USER_PASS='omv'

echo 'add user'

adduser $USER:$USER_PASS:1001:1001:$USER:/home/$USER:/bin/bash
usermod -a -G sudo $USER
echo "$USER:$USER_PASS" | chpasswd

## Set password for root user

echo 'Set root user pass'
echo "root:toor" | chpasswd

#config network
echo "
allow-hotplug eth0
iface eth0 inet dhcp
" > /etc/network/interfaces.d/eth0

## установка ядра и загрузчика
#ARCH=arm64 #варианты: i386, i486, i686, amd64, arm64

## Debian:
#apt-get -y install linux-base linux-image-$ARCH linux-headers-$ARCH


## уставновка большинства прошивок
#apt-get -y install firmware-linux firmware-ralink firmware-realtek
