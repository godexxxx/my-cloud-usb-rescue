#!/bin/sh
mdev -s
umount /mnt/root
mkfs.ext4 -F -L system /dev/mmcblk0p2
mount /dev/mmcblk0p2 /mnt/root
cd /mnt/root
tar zxfv /mnt/usb_top/deb_root.tar.gz